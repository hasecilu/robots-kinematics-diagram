#!/bin/bash

makefile(){
	echo "Enter the file name: "  
	read file_name
	sketch -T "$file_name.sk" -o "$file_name.tex" && \
	pdflatex "$file_name.tex" && \
	xdg-open "$file_name.pdf"
	
	echo "Removing temporary files..."

	rm "$file_name.log"
	rm "$file_name.aux"
}

makefile


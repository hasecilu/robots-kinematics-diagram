# Robots kinematics diagram

Some sketches of robots created with sketch-lib from Alex D

## Usage

- Install [sketch-lib](https://github.com/alexdu/sketch-lib) from Alex D
- Create a *.sk file, (take some examples as reference)
- Give execution permissions to the file `chmod +x sketch2pdf.sh`
- Use the sketch2pdf.sh script

	- `./sketch2pdf.sh`

	- ```
		Enter the filename:
		filename
		```
- You can open the *.tex file to change the color

## Result

![kinematics5](kinematics5.png)

![kinematics2](kinematics2.png)

![kinematics3](kinematics3.png)
